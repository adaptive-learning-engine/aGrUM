Tutorials on pyAgrum
====================

.. nbgallery::
    :glob:

    notebooks/*-Tutorial*
    
Inference in Bayesian networks
==============================

.. nbgallery::
    :glob:

    notebooks/*-Inference*


Learning Bayesian networks
==========================

.. nbgallery::
    :glob:

    notebooks/*-Learning*

Different Graphical Models
==========================

.. nbgallery::
    :glob:

    notebooks/*-Models*

Bayesian networks as scikit-learn compliant classifiers
=======================================================

.. nbgallery::
    :glob:

    notebooks/*-Classifier*

Causal Bayesian Networks
========================

.. nbgallery::
    :glob:

    notebooks/*-Causality*

Examples
========

.. nbgallery::
    :glob:

    notebooks/*-Examples*

pyAgrum's specific features
===========================

.. nbgallery::
    :glob:

    notebooks/*-Tools*

